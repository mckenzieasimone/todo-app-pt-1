import React from "react";
import Todoitems from "./Todoitem";
const Todolist = ({ todos, handleToggle, handleDelete }) => {
  return (
    <secton className="main">
      <ul className="todo-list">
        {todos.map((todo) => (
          <Todoitems
            handleToggle={handleToggle(todo.id)}
            handleDelete={handleDelete(todo.id)}
            id={todo.id}
            title={todo.title}
            completed={todo.completed}
          />
        ))}
      </ul>
    </secton>
  );
};
export default Todolist;
