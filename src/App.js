//test
import React, { useState } from "react";
import {Route, NavLink, Switch} from "react-router-dom";
import Todolist from "./Components/Todolist";
import todosList from "./todos.json";
const App = () => {
  const [todos, setTodos] = useState(todosList);
  const [inputText, setInputText] = useState("");
  const handleAddTodo = (e) => {
    if (e.key === "Enter") {
      const newTodos = [...todos];
      let newtodo = {
        userId: 1,
        id: Math.random() * 12000,
        title: inputText,
        completed: false,
      };
      newTodos.push(newtodo);
      setTodos(newTodos);
      setInputText("");
    }
  };
  const handleToggle = (id) => (event) => {
    let newTodo = todos.map((todo) => {
      return todo.id === id
        ? { ...todo, completed: !todo.completed }
        : { ...todo };
    });
    setTodos(newTodo);
  };
  const handleDelete = (id) => (event) => {
    let hideTodo = todos.filter((todo) => todo.id !== id);
    setTodos(hideTodo);
  };
  const clearCompleted = () => {
    console.log("test");
    let clearComp = todos.filter((todo) => todo.completed === false);
    setTodos(clearComp);
  };
  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input
          className="new-todo"
          placeholder="What needs to be done?"
          autofocus
          onChange={(event) => setInputText(event.target.value)}
          onKeyDown={handleAddTodo}
        />
      </header>
      <Switch>
        <Route
          exact
          path="/all"
          render={(props) => (
            <Todolist
              {...props}
              todos={todos}
              handleToggle={handleToggle}
              handleDelete={handleDelete}
            />
          )}
        />
        <Route
          exact
          path="/active"
          render={(props) => (
            <Todolist
              {...props}
              todos={todos.filter((todo) => todo.completed === false)}
              handleToggle={handleToggle}
              handleDelete={handleDelete}
            />
          )}
        />
        <Route
          exact
          path="/completed"
          render={(props) => (
            <Todolist
              {...props}
              todos={todos.filter((todo) => todo.completed === true)}
              handleToggle={handleToggle}
              handleDelete={handleDelete}
            />
          )}
        />
      </Switch>
      <footer className="footer">
        <span className="todo-count">
      <strong>{todos.filter((todo)=>!todo.completed).length}</strong>item(s) left
        </span>
        <ul className="filters">
          <li>
            <NavLink to="/all" activeClassName="selected">all</NavLink>
          </li>
          <li>
            <NavLink to="/active" activeClassName="selected">active</NavLink>
          </li>
          <li>
            <NavLink to="/completed" activeClassName="selected">completed</NavLink>
          </li>
        </ul>
        <button onClick={(e) => clearCompleted()} className="clear-completed">
          Clear completed
        </button>
      </footer>
    </section>
  );
};
export default App;
